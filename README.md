# Synopsis 

This is my personal Guix channel.

# Installation

[https://www.gnu.org/software/guix/download/](Install) Guix.

My packages can be added as a [[https://www.gnu.org/software/guix/manual/en/html_node/Channels.html][Guix channel]].  To do so, add it to
`~/.config/guix/channels.scm`:

```scheme
  (cons* (channel
          (name 'nullradix)
          (url "https://gitlab.com/mjbecze/guix-channel.git"))
         %default-channels)
```

Then run `guix pull`.